Here an online Rss app based on Cnn podcasts used to run on desktop devices.
Below the list of features:

- feed list at menu left with keyboard navigation

- video frame at right side with keyboard manipulation

- tittle description tags rendered dinamically


Keyboard manipulation:
 
 -arrow up: move up into feed list
 -arrow down: move down into feed list
 -enter key: load video and reproduce it
 -arrow right: change and load channel to the right order(only five channels from CNN are available to reproduce in this app)
 -arrow left : change and load channel to the left order(only five channels from CNN are available to reproduce in this app)


/********************************************************************************************************************/
/********************************************************************************************************************/
technologies used to implement the App:

-Front End:(basically the js's was splitted to give more modulability)
   -Jwplayer api (used to play video podcast)(NOT implemented on FFX)
   -Extjs api(used to manipulate dom and render components)
   -Jquery api (used to give support to Jwplayer api)
   -*html5 cache(used to give offline contents)
   -gloggle css3 styles(implemented on feed list)
   -Asp.net MVC 3  (model  view controller)

-Back End:
  -ServiceStack.Net (An open source api to handle REST services)
  -C#		
/********************************************************************************************************************/
/********************************************************************************************************************/


* html5 content are supported depending of the current browser version, to see that in action, this app would be hosted in a remote server as well i think.(not all content are cached in this app)
* Cnn podcasts provide video files with .m4v extention, on FFX browser this file extention aparently aren't supported yet, 
  by this reason the behavior on this environment should be a quite diferent showing a download dialog instead.


** TO TEST THAT:
	-OPEN  THE SOLUTION WITH VISUAL STUDIO(I'VE USED VS2010),COMPILE THE SOLUTION. **ENSURE TO HAVE PREVIOULSY INSTALLED THE MVC3 FRAMEWORK.

	-PRESS F5: THE MAIN.HTML SHOULD APPEAR ON THE BROWSER PRESELECTED.

	--TO  GET CHANNEL INFO FROM A REST ENDPOINT, JUST REPLACE THE URL:(http://localhost:62009/main.html) by (http://localhost:62009/api/channel)
	  OR IF YOU WANT TO GET AN ESPECIFIC CHANNEL INFO PUT THIS URL (http://localhost:62009/api/channel/0)
	  *the parameter run from 0 to 4, e. g:channel/4:
					      -this value should get the "The CNN Daily" channel.
					      -may you should replace the port(62009) by the port that vs2010 give to you to load the page
					      	







Regards,
Pablo