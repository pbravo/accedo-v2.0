﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using Accedo.ServiceInterface;
using System.Xml.Linq;
using System.IO;
using System.Net;

namespace Accedo
{
    
    public static class Utilities
    {
        public static int channelCurrentPosition = 0;

        public static Dictionary<string, string> rssChannels = new Dictionary<string, string>()
        {
            {"Anderson Cooper 360° Daily", "http://rss.cnn.com/services/podcasting/ac360/rss.xml"},
            {"CNN News Update","http://rss.cnn.com/services/podcasting/newscast/rss.xml"},
            {"CNN Student News","http://rss.cnn.com/services/podcasting/studentnews/rss.xml"},
            {"CNN Profiles","http://rss.cnn.com/services/podcasting/cnnprofiles/rss.xml"},
            {"The CNN Daily","http://rss.cnn.com/services/podcasting/cnnnewsroom/rss.xml"}
        };

        public enum Direction
        {
            Left = 37,
            Right = 39,
            Up = 38,
            Down = 40
        }

        public static List<Channel> GetChannelList(Dictionary<string, string> rssChannels)
        {
            var channelList = new List<Channel>();

            foreach (var channel in rssChannels)
            {
                channelList.Add(GetChannel(channel.Key, channel.Value));
            }

            return channelList;
        }

        public static Channel GetChannel(string channelTittle, string xmlUrl)
        {
            WebClient wc = new WebClient();
            string xmlStr = wc.DownloadString(xmlUrl);
            xmlStr = xmlStr.Replace("<rss version=\"2.0\">", "<Response><rss version=\"2.0\">");

            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlStr);
            XmlNodeList xnFeedList = xml.SelectNodes("//item");

            var channel = new Channel();
            channel.tittle = channelTittle;
            channel.position = Utilities.channelCurrentPosition;
            channel.feedInfoList = new List<FeedInfo>();

            foreach (XmlNode xn in xnFeedList)
            {
                channel.feedInfoList.Add(new FeedInfo()
                {
                    description = GetDescription(xn["description"].InnerText),
                    tittle = xn["title"].InnerText,
                    videoLink = xn["link"].InnerText,
                    pubdate = xn["pubDate"].InnerText
                });
            }

            return channel;
        }

        public static int GetCurrentPosition(int currentPosition, Utilities.Direction direction)
        {
            switch (direction)
            {
                case Utilities.Direction.Right:
                    currentPosition = currentPosition == 4
                        ? 0
                        : (currentPosition >= 0 && currentPosition <= 4 ? currentPosition + 1 : 0);
                    break;
                case Utilities.Direction.Left:
                    currentPosition = currentPosition == 0
                        ? 4
                        : (currentPosition >= 0 && currentPosition <= 4 ? currentPosition - 1 : 4);
                    break;
                default:
                    currentPosition = 0;
                    break;
            }

            return currentPosition;
        }

        private static string GetDescription(string description)
        {
            try
            {
                var indexOfDiv = description.IndexOf("<div", 0);
                var indexOfImg = description.IndexOf("<img", 0);

                return indexOfDiv > 0 
                    ? description.Substring(0, indexOfDiv)
                    : description.Substring(0, indexOfImg);
            }
            catch (Exception)
            {
                return description.Substring(0,30);
            }

        }
    }



}