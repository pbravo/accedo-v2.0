﻿using ServiceStack.Mvc;
using ServiceStack.Mvc.MiniProfiler;
using System.Web.Mvc;
using Accedo.ServiceInterface;
using Accedo.App_Start;
using ServiceStack.WebHost.Endpoints;
using ServiceStack.Text;

namespace Accedo.Controllers
{
    public class RssController : ServiceStackController
    {

        public JsonResult GetChannel(int currentPosition, int direction)
        {
            var channelService = AppHost.Resolve<ChannelService>();

            var response = channelService.GetChannelByDirection(currentPosition, (Utilities.Direction)direction);

            return new ServiceStackJsonResult
            {
                Data = response,
                ContentType = null,
                ContentEncoding = null
            }; 
        }

    }

    public class ServiceStackJsonResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            var response = context.HttpContext.Response;
            response.ContentType = !string.IsNullOrEmpty(ContentType) ? ContentType : "application/json";

            if (ContentEncoding != null)
            {
                response.ContentEncoding = ContentEncoding;
            }

            if (Data != null)
            {
                response.Write(JsonSerializer.SerializeToString(Data));
            }
        }
    }
}
