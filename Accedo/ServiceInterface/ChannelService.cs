﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using ServiceStack.Configuration;
using ServiceStack.OrmLite;
using ServiceStack.Common;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.WebHost.Endpoints;
using System.Runtime.Serialization;

namespace Accedo.ServiceInterface
{

    public class ChannelService : RestServiceBase<Channel>
    {
        public override object OnGet(Channel request)
        {
            if (request.position != default(int))
                return new ChannelRepository().GetByPosition(request.position);

            return new ChannelRepository().GetAll();
        }

        public override object OnPost(Channel request)
        {
            return new ChannelRepository().GetAll();
        }

        public object GetChannelByDirection(int currentPosition, Utilities.Direction direction)
        {
            currentPosition = Utilities.GetCurrentPosition(currentPosition, direction);
            Utilities.channelCurrentPosition = currentPosition;

            return
                new ChannelRepository().GetByPosition(currentPosition);
        }

        public object GetChannelByDirection(int currentPosition)
        {
            Utilities.channelCurrentPosition = currentPosition;

            return
                 new ChannelRepository().GetByPosition(currentPosition);
        }

        public object GetAll()
        {
            return
                new ChannelRepository().GetAll();
        }
    }

    [Route("/channel")]
    [DataContract]
    public class Channel
    {
        [DataMember]
        public int position { get; set; }
        [DataMember]
        public string tittle { get; set; }
        [DataMember]
        public List<FeedInfo> feedInfoList { get; set; }
    }

    public class FeedInfo
    {
        public string description { get; set; }
        public string pubdate { get; set; }
        public string tittle { get; set; }
        public string videoLink { get; set; }
    }

    public class ChannelRepository
    {
        public Dictionary<string, string> channelList = Utilities.rssChannels;

        public List<Channel> GetAll()
        {
            return Utilities.GetChannelList(channelList);
        }

        public Channel GetByPosition(int position)
        {
            return Utilities.GetChannel(Utilities.rssChannels.Keys.ToList()[position], Utilities.rssChannels.Values.ToList()[position]);
        }
    }
}