using System;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using System.Web.Mvc;
using ServiceStack.Configuration;
using ServiceStack.CacheAccess;
using ServiceStack.CacheAccess.Providers;
using ServiceStack.Mvc;
using ServiceStack.OrmLite;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.WebHost.Endpoints;
using Accedo.ServiceInterface;

[assembly: WebActivator.PreApplicationStartMethod(typeof(Accedo.App_Start.AppHost), "Start")]


namespace Accedo.App_Start
{

	public class AppHost: AppHostBase
	{		
		public AppHost() //Tell ServiceStack the name and where to find your web services
			: base("Test base", typeof(ChannelService).Assembly) { }

		public override void Configure(Funq.Container container)
		{
			//Set JSON web services to return idiomatic JSON camelCase properties
			ServiceStack.Text.JsConfig.EmitCamelCaseNames = true;
		
            Routes//REST routes
              .Add<Channel>("/channel")//made by my self...
              .Add<Channel>("/channel/{position}");


            SetConfig(new EndpointHostConfig
            {
                //This is needed since you will be hosting your services from /api
                ServiceStackHandlerFactoryPath = "api"

            });

			//Enable Authentication
			//ConfigureAuth(container);

            container.Register(new ChannelService());//made by my self
			ControllerBuilder.Current.SetControllerFactory(new FunqControllerFactory(container));

		}


		public static void Start()
		{
			new AppHost().Init();
		}
	}
}