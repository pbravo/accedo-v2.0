/**************************************************
author:pablo bravo
date:16/09/2013
company:Accedo
mail:pablo.byte@gmail.com
version: 2.0
***************************************************/

Ext.onReady(function () {

    getChannel(Utilities.feedObject.currentPosition, Utilities.direction.Right);

});

renderPrototype = function (data) {

    // create the data store
    var feedStore = Ext.create('Ext.data.ArrayStore', {
        fields: [
           { name: 'description' },
           { name: 'pubdate' },
           { name: 'renderTittle' },
           { name: 'link' }
        ],
        data: data
    });
    Ext.get('div-body-frame-podcast-list').update();
    var feedGrid = Ext.create('Ext.grid.Panel', {
        id: 'feedGridID',
        store: feedStore,
        stateId: 'stateGrid',
        hideHeaders: true,
        columns: [
            {
                sortable: false,
                renderer: Utilities.renderTittle,
                dataIndex: 'renderTittle',
                width: 500
            }
        ],
        height: 380,
        width: 480,
        renderTo: 'div-body-frame-podcast-list',
        viewConfig: {
            stripeRows: true
        },
        listeners: {
            'cellclick': function (grid, td, cellIndex, record, tr, rowIndex) {
                Utilities.loadFeedVideo(record);
            },
            'cellkeydown': function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
                if (e.keyCode == Utilities.direction.Enter) {
                    Utilities.loadFeedVideo(record);
                    if (!Ext.isGecko)
                        jwplayer().play();
                }
                else if (e.keyCode == Utilities.direction.Right) //right
                    getChannel(Utilities.feedObject.currentPosition, Utilities.direction.Right);
                else if(e.keyCode == Utilities.direction.Left) //left
                    getChannel(Utilities.feedObject.currentPosition, Utilities.direction.Left);
            }

        }
    });
    Utilities.setKeyboardListerner();

    Utilities.setFirstTimeValuesAndListeners(feedGrid);
};


getChannel = function (currentPosition, direction) {

    Ext.Ajax.request({
        url: 'Ajax.ashx/Rss/GetChannel',
        method: 'POST',
        timeout: 400000,
        params: { currentPosition: currentPosition, direction: direction },
        success: function (res, options) {
            if (res.responseText != 'undefined') {
                var rrsChannel = Ext.decode(res.responseText);
                Utilities.feedObject.channelTittle = rrsChannel.tittle; //channel tittle
                Utilities.feedObject.currentPosition = rrsChannel.position; //current position into Rss channel list
                Utilities.feedObject.feedArray = [];

                Ext.each(rrsChannel.feedInfoList, function (feedItem) {
                    Utilities.feedObject.feedArray.push([
	                    feedItem.description, //description
	                    feedItem.pubdate, //pubdate
                        Utilities.getShortNameWithDots(feedItem.description, 30) + feedItem.pubdate, //feed tittle + pubdate
	                    feedItem.tittle, //feed tittle
	                    feedItem.videoLink//link
	                ]);
                });

                renderPrototype(Utilities.feedObject.feedArray);
            }
        },
        failure: function () {

            alert('get rss fail, please try refreshing page!');
        }
    });
}


