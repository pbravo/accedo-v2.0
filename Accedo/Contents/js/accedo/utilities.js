﻿var Utilities = {

    feedObject: {
        feedArray: [],
        channelTittle: '',
        currentPosition: -1
    },
    getShortNameWithDots: function (name, maxLenght) {

        if (name.length > maxLenght) {
            return name.substring(0, maxLenght - 3) + '...';
        }

        return name;
    },
    direction: {
        Up: 38,
        Down: 40,
        Left: 37,
        Right: 39,
        Enter: 13
    },
    updateFeedContent : function (record) {
        Ext.fly('span-tittle-podcast').update(Utilities.feedObject.channelTittle);
        Ext.fly('span-tittle-description').update(record.data.link);
        Ext.fly('span-currentdescription').update(record.data.description);
    },
    renderTittle : function (val) {
        return '<div style="height:50px;"><span class="feed-content-row font-effect-shadow-multiple">' + val + '</span></div>';
    },
    loadFeedVideo : function (record) {
        var currentRecord = record[0] ? record[0] : record;
        var rsslink = currentRecord.data.id;// Ext.isGecko || Ext.isIE ? currentRecord.data.id : currentRecord.data.link;
        Utilities.updateFeedContent(currentRecord);

        if (Ext.isGecko) {//FFX doesn't support natively .m4v files
            $("#div-body-frame-podcast-video").empty();
            $("#div-body-frame-podcast-video").append("<iframe src=" + rsslink + "' style='height:430px; width: 460px;'></iframe>");
        }
        else {

            jwplayer("div-body-frame-podcast-video").setup({
                file: rsslink,
                height: 430,
                image: "images/rss.jpg",
                width: 460
            });
        }
    },
    setKeyboardListerner : function () {
        var charfield = document.getElementById("bodyRssId")
        charfield.onkeydown = function (e) {

            var e = window.event || e;

            if (Ext.getCmp('feedGridID')) {
                var grid = Ext.getCmp('feedGridID');

                switch (e.keyCode) {
                    case Utilities.direction.Down: //down
                        grid.getSelectionModel().selectNext();
                        break;
                    case Utilities.direction.Up: //up
                        grid.getSelectionModel().selectPrevious();
                        break;
                    case Utilities.direction.Enter: //enter
                        var record = grid.getSelectionModel().getSelection();
                        Utilities.loadFeedVideo(record);
                        if (!Ext.isGecko)
                            jwplayer().play();
                        break;
                    case Utilities.direction.Right: //right
                        getChannel(Utilities.feedObject.currentPosition, Utilities.direction.Right);
                        break;
                    case Utilities.direction.Left: //left
                        getChannel(Utilities.feedObject.currentPosition, Utilities.direction.Left);
                        break;

                }
            }
        }
    },
    setFirstTimeValuesAndListeners : function (feedGrid) {

        feedGrid.getSelectionModel().select(0);
        var record = feedGrid.getSelectionModel().getSelection();
        Utilities.loadFeedVideo(record);

        Ext.get('div-click-up').on('click', function (e) {
            feedGrid.getSelectionModel().selectPrevious();
        });
        Ext.get('div-click-down').on('click', function (e) {
            feedGrid.getSelectionModel().selectNext();
        });
    }
};

